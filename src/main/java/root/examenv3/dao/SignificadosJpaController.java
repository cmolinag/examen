/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenv3.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.examenv3.dao.exceptions.NonexistentEntityException;
import root.examenv3.dao.exceptions.PreexistingEntityException;
import root.examenv3.entity.Significados;

/**
 *
 * @author carlo
 */
public class SignificadosJpaController implements Serializable {

    public SignificadosJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Significados significados) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(significados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSignificados(significados.getDbpalabra()) != null) {
                throw new PreexistingEntityException("Significados " + significados + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Significados significados) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            significados = em.merge(significados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = significados.getDbpalabra();
                if (findSignificados(id) == null) {
                    throw new NonexistentEntityException("The significados with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Significados significados;
            try {
                significados = em.getReference(Significados.class, id);
                significados.getDbpalabra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The significados with id " + id + " no longer exists.", enfe);
            }
            em.remove(significados);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Significados> findSignificadosEntities() {
        return findSignificadosEntities(true, -1, -1);
    }

    public List<Significados> findSignificadosEntities(int maxResults, int firstResult) {
        return findSignificadosEntities(false, maxResults, firstResult);
    }

    private List<Significados> findSignificadosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Significados.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Significados findSignificados(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Significados.class, id);
        } finally {
            em.close();
        }
    }

    public int getSignificadosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Significados> rt = cq.from(Significados.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
