/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenv3.dto;

import java.util.ArrayList;

/**
 *
 * @author carlo
 */
public class PalabraDTO {
    
  private String id;
  Metadata MetadataObject;
  ArrayList<Object> results = new ArrayList<Object>();
  private String word;


 // Getter Methods 

  public String getId() {
    return id;
  }

  public Metadata getMetadata() {
    return MetadataObject;
  }

  public String getWord() {
    return word;
  }

 // Setter Methods 

  public void setId( String id ) {
    this.id = id;
  }

  public void setMetadata( Metadata metadataObject ) {
    this.MetadataObject = metadataObject;
  }

  public void setWord( String word ) {
    this.word = word;
  }
}
public class Metadata {
  private String operation;
  private String provider;
  private String schema;


 // Getter Methods 

  public String getOperation() {
    return operation;
  }

  public String getProvider() {
    return provider;
  }

  public String getSchema() {
    return schema;
  }

 // Setter Methods 

  public void setOperation( String operation ) {
    this.operation = operation;
  }

  public void setProvider( String provider ) {
    this.provider = provider;
  }

  public void setSchema( String schema ) {
    this.schema = schema;
  }
}
    

