/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenv3.dto;

import java.util.List;

/**
 *
 * @author Ripley
 */
public class Entry {

    /**
     * @return the etymologies
     */
    public List<String> getEtymologies() {
        return etymologies;
    }

    /**
     * @param etymologies the etymologies to set
     */
    public void setEtymologies(List<String> etymologies) {
        this.etymologies = etymologies;
    }

    /**
     * @return the grammaticalFeatures
     */
    public List<GrammaticalFeature> getGrammaticalFeatures() {
        return grammaticalFeatures;
    }

    /**
     * @param grammaticalFeatures the grammaticalFeatures to set
     */
    public void setGrammaticalFeatures(List<GrammaticalFeature> grammaticalFeatures) {
        this.grammaticalFeatures = grammaticalFeatures;
    }

    /**
     * @return the senses
     */
    public List<Sens> getSenses() {
        return senses;
    }

    /**
     * @param senses the senses to set
     */
    public void setSenses(List<Sens> senses) {
        this.senses = senses;
    }
    private List<String> etymologies;
    private List<GrammaticalFeature> grammaticalFeatures;
    private List<Sens> senses; 
    
}
