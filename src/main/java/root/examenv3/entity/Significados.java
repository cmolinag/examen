/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.examenv3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author carlo
 */
@Entity
@Table(name = "significados")
@NamedQueries({
    @NamedQuery(name = "Significados.findAll", query = "SELECT s FROM Significados s"),
    @NamedQuery(name = "Significados.findByDbpalabra", query = "SELECT s FROM Significados s WHERE s.dbpalabra = :dbpalabra"),
    @NamedQuery(name = "Significados.findByDbsignificado", query = "SELECT s FROM Significados s WHERE s.dbsignificado = :dbsignificado")})
public class Significados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "dbpalabra")
    private String dbpalabra;
    @Size(max = 2147483647)
    @Column(name = "dbsignificado")
    private String dbsignificado;

    public Significados() {
    }

    public Significados(String dbpalabra) {
        this.dbpalabra = dbpalabra;
    }

    public String getDbpalabra() {
        return dbpalabra;
    }

    public void setDbpalabra(String dbpalabra) {
        this.dbpalabra = dbpalabra;
    }

    public String getDbsignificado() {
        return dbsignificado;
    }

    public void setDbsignificado(String dbsignificado) {
        this.dbsignificado = dbsignificado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dbpalabra != null ? dbpalabra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Significados)) {
            return false;
        }
        Significados other = (Significados) object;
        if ((this.dbpalabra == null && other.dbpalabra != null) || (this.dbpalabra != null && !this.dbpalabra.equals(other.dbpalabra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.examenv3.entity.Significados[ dbpalabra=" + dbpalabra + " ]";
    }
    
}
